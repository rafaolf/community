#!/bin/bash

timestamp() {
  date '+%Y-%m-%d.%H:%M:%S'
}

CURRENT_DATE=`timestamp`
docker-compose exec mariadb /usr/bin/mysqldump -u root --password=root d7 > ../docroot/backups/backup-$CURRENT_DATE.sql
